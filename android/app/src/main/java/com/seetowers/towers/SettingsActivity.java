package com.seetowers.towers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SettingsActivity extends AppCompatActivity {

    public float convertDpToPixel(float dp){
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public class StateListAdapter extends ArrayAdapter<String[]> {

        private final Activity context;
        private List<String[]> states;
        private String hash;

        public StateListAdapter(Activity context) {
            super(context, R.layout.state_list_item);
            this.context = context;
        }

        public void readData() {
            refresh();

            final ProgressDialog loading = new ProgressDialog(SettingsActivity.this);
            if (!SettingsActivity.this.isFinishing() && states.isEmpty()) {
                loading.setCancelable(true);
                loading.setMessage("loading...");
                loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                loading.show();
            }

            hash = sharedPref.getString(SeetowersClient.HASH_KEY, "");
            SeetowersClient client = new SeetowersClient();
            client.getStates(hash, SettingsActivity.this, new SeetowersClient.SuccessListener() {
                public void run(Map<String, String> result) {
                    if (result.containsKey(SeetowersClient.STATES_KEY)) {
                        sharedPref.edit()
                                .putString(SeetowersClient.STATES_KEY, result.get(SeetowersClient.STATES_KEY))
                                .putString(SeetowersClient.HASH_KEY, result.get(SeetowersClient.HASH_KEY))
                                .apply();
                        refresh();
                    }
                }
            }, new SeetowersClient.ErrorListener() {
                @Override
                public void run(String result) {
                    if (!SettingsActivity.this.isFinishing()) {
                        new AlertDialog.Builder(SettingsActivity.this)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).setTitle("Error").setMessage(result).create().show();
                    }
                }
            }, new Runnable() {
                @Override
                public void run() {
                    if (!SettingsActivity.this.isFinishing() && loading.isShowing()) {
                        loading.dismiss();
                    }
                }
            });
        }

        private void refresh() {
            String statesJSON = sharedPref.getString("states", "[]");
            states = new ArrayList<>();
            try {
                JSONArray all = new JSONArray(statesJSON);
                for (int i = 0; i < all.length(); i++) {
                    try {
                        JSONArray rawState = all.getJSONArray(i);
                        String[] state = new String[] {rawState.getString(0), rawState.getString(1)};
                        states.add(state);
                    } catch (JSONException ex) {
                        Log.e("Parse", "Error parsing state", ex);
                    }
                }
            } catch (JSONException ex) {
                sharedPref.edit().remove(SeetowersClient.HASH_KEY).apply();
                Log.e("Parse", "Error parsing towers: " + statesJSON, ex);
            }
            clear();
            addAll(states);

            ViewGroup.LayoutParams params = stateList.getLayoutParams();
            params.height = (int)convertDpToPixel(50) * states.size() + 20;
            stateList.setLayoutParams(params);
            stateList.requestLayout();
        }

        public View getView(final int position, View view, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.state_list_item, null, true);

            Button button = (Button) rowView.findViewById(R.id.stateCode);
            button.setText(states.get(position)[1]);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String state = states.get(position)[0];
                    sharedPref.edit().putString("state", state).apply();
                    NavUtils.navigateUpFromSameTask(SettingsActivity.this);
                }
            });
            return rowView;
        };
    }

    private ListView stateList;
    private StateListAdapter stateListAdapter;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView about = ((TextView)findViewById(R.id.about));
        about.setText(Html.fromHtml("seetowers.com app v0.0.2a<br/>"
                        + "Questions? <a href='https://seetowers.com/'>seetowers.com</a><br/>"
                        + "Philip Peshin (c) 2018"
        ));
        about.setMovementMethod(LinkMovementMethod.getInstance());

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Units units = Units.safeValueOf(sharedPref.getString("units", Units.mi.name()));
        ((RadioButton) findViewById(R.id.unitsKm)).setChecked(units == Units.km);
        ((RadioButton) findViewById(R.id.unitsMi)).setChecked(units == Units.mi);

        stateListAdapter = new StateListAdapter(this);
        stateList = (ListView)findViewById(R.id.stateList);
        stateList.setAdapter(stateListAdapter);
        stateListAdapter.readData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onUnitsClicked(View view) {
        sharedPref.edit()
                .putString("units", getUnits().name())
                .apply();
    }

    private Units getUnits() {
        return ((RadioButton) findViewById(R.id.unitsMi)).isChecked()
                ? Units.mi : (((RadioButton) findViewById(R.id.unitsKm)).isChecked()
                ? Units.km :  Units.mi);
    }

    public static final double metersInMile = 1609.344;
    public static final double metersInKm = 1000.00;
    private static final double feetInMeter = 3.2808399;

    private String toUnits(int value) {
        String result = String.valueOf(value);
        switch (getUnits()) {
            case mi:
                result = String.format("%.3f", value/metersInMile);
                break;
            case km:
                result = String.format("%.3f", value/metersInKm);
                break;
            case ft:
                result = String.format("%d", (int)Math.round(feetInMeter * value));
                break;
            default:
                break;
        }
        return result;
    }

    private int fromUnits(String value) {
        int result;
        switch (getUnits()) {
            case mi:
                result = (int) Math.round(metersInMile * Double.parseDouble(value));
                break;
            case km:
                result = (int) Math.round(metersInKm * Double.parseDouble(value));
                break;
            case ft:
                result = (int) Math.round(Double.parseDouble(value) / feetInMeter);
                break;
            default:
                result = (int) Math.round(1 * Double.parseDouble(value));
                break;
        }
        return result;
    }
}
