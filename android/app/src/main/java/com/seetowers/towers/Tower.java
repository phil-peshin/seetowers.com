package com.seetowers.towers;

class Tower {
    public double latitude;
    public double longitude;
    public String name;
    public String city;
    public String state;
    public double azimuth;
    public double distance;
    public double x;
    public double y;
    public boolean disabled;

    public Tower(double latitude, double longitude, String name, String city, String state) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.city = city;
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof Tower ? name.equals(((Tower)o).name) && latitude == ((Tower)o).latitude && longitude == ((Tower)o).longitude : false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}