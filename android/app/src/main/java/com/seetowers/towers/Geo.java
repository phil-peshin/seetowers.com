package com.seetowers.towers;

class Geo {
    static double PI                = 3.14159265359;
    static double TWOPI             = 6.28318530718;
    static double DE2RA             = 0.01745329252;
    static double RA2DE             = 57.2957795129;
    static double ERAD              = 6378.135;
    static double ERADM             = 6378135.0;
    static double AVG_ERAD          = 6371.0;
    static double FLATTENING        = 1.0/298.257223563;
    static double EPS               = 0.000000000005;
    static double KM2MI             = 0.621371;
    static double GEOSTATIONARY_ALT = 35786.0;

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double lat1_ = lat1 * DE2RA;
        double lon1_ = lon1 * DE2RA;
        double lat2_ = lat2 * DE2RA;
        double lon2_ = lon2 * DE2RA;
        double d = Math.sin(lat1_) * Math.sin(lat2_) + Math.cos(lat1_) * Math.cos(lat2_) * Math.cos(lon1_ - lon2_);
        return AVG_ERAD * Math.acos(d);
    }

    public static double azimuth(double lat1, double lon1, double lat2, double lon2) {
        double result = 0.0;

        double ilat1 = Math.floor(0.50 + lat1 * 360000.0);
        double ilat2 = Math.floor(0.50 + lat2 * 360000.0);
        double ilon1 = Math.floor(0.50 + lon1 * 360000.0);
        double ilon2 = Math.floor(0.50 + lon2 * 360000.0);

        double lat1_ = lat1 * DE2RA;
        double lon1_ = lon1 * DE2RA;
        double lat2_ = lat2 * DE2RA;
        double lon2_ = lon2 * DE2RA;

        if ((ilat1 == ilat2) && (ilon1 == ilon2)) {
            return result;
        } else if (ilon1 == ilon2) {
            if (ilat1 > ilat2) {
                result = 180.0;
            }
        } else {
            double c = Math.acos(Math.sin(lat2_) * Math.sin(lat1_) + Math.cos(lat2_) * Math.cos(lat1_) * Math.cos((lon2_ - lon1_)));
            double A = Math.asin(Math.cos(lat2_) * Math.sin((lon2_ - lon1_)) / Math.sin(c));
            result = (A * RA2DE);

            if ((ilat2 > ilat1) && (ilon2 > ilon1)) {
                // nothing
            } else if ((ilat2 < ilat1) && (ilon2 < ilon1)) {
                result = 180.0 - result;
            } else if ((ilat2 < ilat1) && (ilon2 > ilon1)) {
                result = 180.0 - result;
            } else if ((ilat2 > ilat1) && (ilon2 < ilon1)) {
                result += 360.0;
            }
        }
        return result;
    }

}