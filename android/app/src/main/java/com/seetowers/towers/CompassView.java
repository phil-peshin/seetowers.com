package com.seetowers.towers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ppeshin on 3/3/16.
 */
public class CompassView extends View {

    public double mBearing = Double.NaN;

    public double mCurrentRotation = 0;

    public int mAccuracy = 0;

    public boolean headingUpMode = true;

    public TowersView towersView;

    private GestureDetector mTapDetector;

    private int[] accuracyColors = new int[]{Color.GRAY, Color.RED, Color.YELLOW, Color.GREEN};

    public CompassView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mTapDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                headingUpMode = !headingUpMode;
                invalidate();
                towersView.invalidate();
                return super.onSingleTapConfirmed(e);
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        Point center = new Point(canvas.getWidth()/2, canvas.getHeight() / 2);
        float radius = Math.min(center.x, center.y);


        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);

        canvas.rotate(getCurrentAngle(), radius, radius);
        Path path = new Path();

        if (headingUpMode) {
            path.reset();
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            path.addCircle(center.x, center.y, radius / 3, Path.Direction.CW);
            paint.setColor(accuracyColors[mAccuracy]);
            canvas.drawPath(path, paint);
        }

        path.reset(); // only needed when reusing this path for a new build
        path.moveTo(radius, 2.0f); // used for first point
        path.lineTo(6.0f / 5.0f * radius, radius);
        path.lineTo(4.0f / 5.0f * radius, radius);
        path.lineTo(radius, 2.0f);
        paint.setColor(Color.RED);
        canvas.drawPath(path, paint);

        path.reset(); // only needed when reusing this path for a new build
        path.moveTo(radius, 2 * radius - 2.0f); // used for first point
        path.lineTo(6.0f / 5.0f * radius, radius);
        path.lineTo(4.0f / 5.0f * radius, radius);
        path.lineTo(radius, 2 * radius - 2.0f);
        paint.setColor(Color.WHITE);
        canvas.drawPath(path, paint);

        canvas.restore();
    }

    public float getCurrentAngle() {
        return -(float) (headingUpMode ? mBearing : mCurrentRotation);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mTapDetector.onTouchEvent(event);
        return true;
    }

    public void rotate(float angle) {
        headingUpMode = false;
        mCurrentRotation = angle;
        invalidate();
    }
}
