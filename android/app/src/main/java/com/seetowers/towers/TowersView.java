package com.seetowers.towers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ppeshin on 3/3/16.
 */
public class TowersView extends View {

    private float maxDistance = 5;
    private float currentScale = 1;
    private float currentRotation = 0;

    public Set<Tower> visibleTowers = new HashSet<Tower>();
    public Tower target;

    public CompassView compassView;
    public Units units = Units.mi;

    private static final int darkYellowColor = Color.rgb(233, 172, 58);
    private static final int lightBlueColor = Color.rgb(0x64, 0x95, 0xED);
    private static final int lightGrayColor = Color.rgb(0x80, 0x80, 0x80);
    private static final int orangeColor = Color.rgb(255,165,0);

    private ScaleGestureDetector mScaleDetector;
    private RotationGestureDetector mRotationDetector;
    private GestureDetector mTapDetector;

    public TowersView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                currentScale *= detector.getScaleFactor();
                currentScale = Math.max(0.05f, Math.min(currentScale, 2.0f));
                Log.d("Scale", "scale=" + currentScale);
                ((MainActivity)getContext()).currentScale = currentScale;
                ((MainActivity)getContext()).processTowers();
                invalidate();
                return true;
            }
        });

        mRotationDetector = new RotationGestureDetector(new RotationGestureDetector.OnRotationGestureListener() {
            @Override
            public void onRotation(RotationGestureDetector rotationDetector, boolean start) {
                if (start) {
                    currentRotation = -getCurrentAngle();
                    rotationDetector.setStartAngle(currentRotation);
                } else {
                    currentRotation = rotationDetector.getAngle();
                    compassView.rotate(currentRotation);
                    invalidate();
                }
            }
        });

        mTapDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

            private Set<Tower> getTowersAt(MotionEvent e) {
                float radius = 2.f/3.f * getHeight();
                float x0 = getWidth() / 2;
                float y0 = radius;
                float transformScale = radius / maxDistance * currentScale;

                float[] pt = {e.getX(), e.getY()};
                matrix.setRotate(-getCurrentAngle(), x0, y0);
                matrix.mapPoints(pt);

                float x = (pt[0] - x0)/transformScale;
                float y = (y0 - pt[1])/transformScale;
                float circleRadius = 1.2f * getCircleRadius()/transformScale;

                Set<Tower> result = new HashSet<>();
                for (Tower tower : visibleTowers) {
                    if (Math.abs(tower.x - x) <= circleRadius && Math.abs(tower.y - y) <= circleRadius) {
                        result.add(tower);
                    }
                }
                return result;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                Set<Tower> towers = getTowersAt(e);
                if (!towers.isEmpty()) {
                    Tower newTarget = towers.iterator().next();
                    if (newTarget == target) {
                        target = null;
                    } else {
                        target = newTarget;
                    }
                    invalidate();
                }
                return super.onSingleTapConfirmed(e);
            }
        });

        // to avoid rendered thread crash
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private float getCircleRadius() {
        return Math.min(getWidth(), getHeight())/40f;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);
        mRotationDetector.onTouchEvent(event);
        mTapDetector.onTouchEvent(event);
        return true;
    }

    private Paint paint = new Paint();
    private Path path = new Path();
    private Matrix matrix = new Matrix();

    private double MIN_SPEED = 10;
    private static final float[] scales = new float[] {
        1.00f, 1,
        0.30f, 2,
        0.15f, 5,
        0.07f, 10,
        0.00f, 20
    };

    private float scalingStep(float distanceCoeff, int cir) {
        for (int i = 0; i < scales.length; i += 2) {
            if (currentScale >= scales[i]/distanceCoeff) {
                return cir * scales[i + 1];
            }
        }
        return cir * scales[scales.length - 1];
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();

        // this is main unit for screen sizes
        float circleRadius = getCircleRadius();
        float normalFontSize = 1.33f * circleRadius;
        float largeFontSize = 1.66f * circleRadius;

        paint.reset();
        path.reset();

        float textX = 12 * circleRadius;
        float textH = 2.2f * circleRadius;
        float textY = textH / 2;

        float distanceCoeff;
        String distanceUnits;
        double speedCoeff;
        String speedUnits;
        switch (units) {
            case mi:
            case ft:
                distanceCoeff = 1.609344f;
                distanceUnits = Units.mi.name();
                speedCoeff = 1.609344;
                speedUnits = "mph";
                break;
            case km:
            case m:
            default:
                distanceCoeff = 1;
                distanceUnits = Units.km.name();
                speedCoeff = 1;
                speedUnits = "km/h";
                break;
        }

        float radius = 2.f/3.f * getHeight();
        float x0 = getWidth() / 2.f;
        float y0 = radius;

        float transformScale = radius / maxDistance * currentScale;

        paint.setColor(lightGrayColor);
        paint.setTextSize(normalFontSize);
        paint.setTypeface(Typeface.create("Helvetica Bold", Typeface.NORMAL));

        for (int cir = 1; cir <= 8; cir++) {
            float rr = scalingStep(distanceCoeff, cir);
            path.reset();
            paint.setStyle(Paint.Style.STROKE);
            path.addCircle(x0, y0, rr * distanceCoeff * transformScale, Path.Direction.CW);
            canvas.drawPath(path, paint);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            canvas.drawText(String.format("%d %s", (int)rr, distanceUnits), x0 - circleRadius, y0 - circleRadius - rr * distanceCoeff * transformScale, paint);
        }

        canvas.rotate(getCurrentAngle(), x0, y0);
        for (int dir = 0; dir < 360; dir += 30) {
            path.reset();
            paint.setStyle(Paint.Style.STROKE);
            float rr1 = scalingStep(distanceCoeff, 1);
            float rr2 = scalingStep(distanceCoeff, 8);
            double sinDir = Math.sin(Math.toRadians(dir));
            double cosDir = Math.cos(Math.toRadians(dir));
            float dx1 = (float) (rr1 * sinDir * distanceCoeff * transformScale);
            float dy1 = (float) (rr1 * cosDir * distanceCoeff * transformScale);
            float dx2 = (float) (rr2 * sinDir * distanceCoeff * transformScale);
            float dy2 = (float) (rr2 * cosDir * distanceCoeff * transformScale);

            if (dir == 0) {
                paint.setColor(Color.WHITE);
            } else if (dir == 180) {
                paint.setColor(Color.RED);
            } else {
                paint.setColor(lightGrayColor);
            }
            path.moveTo(x0 + dx1, y0 + dy1);
            path.lineTo(x0 + dx2, y0 + dy2);
            canvas.drawPath(path, paint);
        }
        canvas.rotate(-getCurrentAngle(), x0, y0);

        matrix.setRotate(getCurrentAngle(), x0, y0);
        for (Tower tower : visibleTowers) {
            canvas.rotate(getCurrentAngle(), x0, y0);

            double xx = x0 + tower.x * transformScale;
            double yy = y0 - tower.y * transformScale;

            int color;
            if (tower == target) {
                color = Color.RED;
            } else {
                color = lightBlueColor;
            }

            paint.setColor(color);
            paint.setStrokeWidth(1);

            path.reset();
            paint.setStyle(Paint.Style.FILL);
            path.addCircle((float) xx, (float) yy, circleRadius, Path.Direction.CW);
            canvas.drawPath(path, paint);
            //paint.setStyle(Paint.Style.STROKE);
            //paint.setColor(Color.WHITE);
            //path.addCircle((float) xx, (float) yy, circleRadius, Path.Direction.CW);
            //canvas.drawPath(path, paint);

            double angle = getCurrentAngle() * Math.PI/180f;
            double xxx = xx + 1.5 * circleRadius * Math.cos(angle);
            double yyy = yy - 1.5 * circleRadius * Math.sin(angle);
            int maxNameLength = 24;
            String name = tower.name;
            if (tower.name.length() > maxNameLength ) {
                name = name.substring(0, maxNameLength) + "...";
            }
            float[] pts = {(float)xxx, (float)yyy};
            matrix.mapPoints(pts);
            canvas.rotate(-getCurrentAngle(), x0, y0);
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            paint.setTextSize(24);
            paint.setTypeface(Typeface.create("Helvetica", Typeface.NORMAL));
            canvas.drawText(name, pts[0], pts[1], paint);
        }
        canvas.restore();
    }

    public float getCurrentAngle() {
        return compassView != null ? compassView.getCurrentAngle() : 0;

    }
}
