package com.seetowers.towers;

/**
 * Created by ppeshin on 3/17/16.
 */
public enum Units {
    m,
    mi,
    km,
    ft;

    public static Units safeValueOf(String n) {
        try {
            return valueOf(n);
        }  catch (Exception ex) {
            return m;
        }
    }
}
