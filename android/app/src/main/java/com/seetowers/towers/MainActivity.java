package com.seetowers.towers;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity implements SensorEventListener, LocationListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private TowersView mTowersView;
    private CompassView mCompassView;
    private SensorManager mSensorManager;
    private LocationManager mLocationManager;
    private Sensor mSensorAccelerometer;
    private Sensor mSensorMagneticField;
    private Sensor mSensorOrientation;

    private float[] mValuesAccelerometer = new float[3];
    private float[] mValuesMagneticField = new float[3];
    private float[] mValuesOrientation = new float[3];
    private float[] mMatrixR = new float[9];
    private float[] mMatrixI = new float[9];
    private float[] mMatrixValues = new float[3];

    private double mMinDiffForEvent = 0.5;
    private double mThrottleTime = 200;
    private long mLastChangeDispatchedAt = -1;

    private long mLastLocationUpdatedAt = -1;
    private long mLocationUpdateThrottle = 1000;

    private AverageAngle mAzimuthRadians = new AverageAngle(10);
    private double mAzimuth = Double.NaN;
    private double mBearing = Double.NaN;
    private double mLastBearing = Double.NaN;


    private Location mLocation;

    private double latitude = 0;
    private double longitude = 0;
    private double maxDistance = 5;
    public double currentScale = 1;

    private String state;
    private String hash;
    private String towersJSON;
    private String metaJSON;
    private String stateName;
    private Set<Tower> allTowers = new HashSet<Tower>();
    private Set<Tower> visibleTowers = new HashSet<Tower>();

    private MediaPlayer visiblePlayer;
    private MediaPlayer inrangePlayer;
    private Vibrator vibrator;
    private ProgressDialog waitingForGPS;
    private SharedPreferences sharedPref;

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initLocationServices();
                } else {
                }
                return;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        int locationPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(MainActivity.this)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setTitle("Location services")
                    .setMessage("This app needs access to your location to identify nearby Reswue alerts")
                    .create().show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } else {
            initLocationServices();
        }

        waitingForGPS = new ProgressDialog(this);
        waitingForGPS.setCancelable(true);
        waitingForGPS.setMessage("Waiting for location...");
        waitingForGPS.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        waitingForGPS.show();

        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorMagneticField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);


        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        mTowersView = (TowersView) findViewById(R.id.fullscreen_content);
        mCompassView = (CompassView) findViewById(R.id.compassView);
        mCompassView.towersView = mTowersView;
        mTowersView.compassView = mCompassView;

        // Set up the user interaction to manually show or hide the system UI.
        mTowersView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });
        findViewById(R.id.menuButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        mTowersView.units = Units.safeValueOf(sharedPref.getString("units", Units.mi.name()));
        sharedPref.registerOnSharedPreferenceChangeListener(this);
        state = sharedPref.getString("state", "");
        if (state.isEmpty()) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        } else {
            refresh();
            retrieveTowers();
        }

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPref, String key) {
        switch (key) {
            case "state":
                state = sharedPref.getString("state", "");
                retrieveTowers();
                break;
            default:
                refresh();
                break;
        }
    }

    private void initLocationServices() {
        try {
            for (final String provider : mLocationManager.getProviders(true)) {
                if (LocationManager.GPS_PROVIDER.equals(provider)
                        || LocationManager.PASSIVE_PROVIDER.equals(provider)
                        || LocationManager.NETWORK_PROVIDER.equals(provider)) {
                    mLocationManager.requestLocationUpdates(provider, 0, 1.0f, this);
                }
            }
        } catch (SecurityException ex) {
            new AlertDialog.Builder(MainActivity.this)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                }).setTitle("Location services are not enabled").setMessage(ex.getMessage()).create().show();
        }
    }

    private void retrieveTowers() {
        hash = sharedPref.getString(SeetowersClient.STATE_HASH(state), "");
        final ProgressDialog loading = new ProgressDialog(MainActivity.this);
        if (!MainActivity.this.isFinishing() && hash.isEmpty()) {
            loading.setCancelable(true);
            loading.setMessage("loading...");
            loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loading.show();
        }

        SeetowersClient client = new SeetowersClient();
        client.getTowers(state, hash, MainActivity.this, new SeetowersClient.SuccessListener() {
            public void run(Map<String, String> result) {
                if (result.containsKey(SeetowersClient.DATA_KEY)) {
                    sharedPref.edit()
                            .putString(SeetowersClient.STATE_DATA(state), result.get(SeetowersClient.DATA_KEY))
                            .putString(SeetowersClient.STATE_HASH(state), result.get(SeetowersClient.HASH_KEY))
                            .putString(SeetowersClient.STATE_META(state), result.get(SeetowersClient.META_KEY))
                            .apply();
                }
            }
        }, new SeetowersClient.ErrorListener() {
            @Override
            public void run(String result) {
                if (!MainActivity.this.isFinishing()) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setTitle("Error").setMessage(result).create().show();
                }
            }
        }, new Runnable() {
            @Override
            public void run() {
                if (!MainActivity.this.isFinishing() && loading.isShowing()) {
                    loading.dismiss();
                }
            }
        });

    }

    private void parseTowers() {
        allTowers.clear();
        if (towersJSON != null) {
            try {
                JSONArray all = new JSONArray(towersJSON);
                for (int i = 0; i < all.length(); i++) {
                    try {
                        JSONArray rawTower = all.getJSONArray(i);
                        Tower tower = new Tower(
                                rawTower.getDouble(0),
                                rawTower.getDouble(1),
                                rawTower.getString(2),
                                rawTower.getString(3),
                                rawTower.getString(4));
                        allTowers.add(tower);
                    } catch (JSONException ex) {
                        Log.e("Parse", "Error parsing tower", ex);
                    }
                }
            } catch (JSONException ex) {
                sharedPref.edit().remove(SeetowersClient.STATE_HASH(state)).apply();
                Log.e("Parse", "Error parsing towers: " + towersJSON, ex);
            }
        }
        if (metaJSON != null) {
            try {
                JSONObject meta = new JSONObject(metaJSON);
                if (meta.has("stateName")) {
                    stateName = meta.getString("stateName");
                    ((TextView) findViewById(R.id.stateName)).setText(stateName);
                }
            } catch (JSONException ex) {
                sharedPref.edit().remove(SeetowersClient.STATE_HASH(state)).apply();
                Log.e("Parse", "Error parsing towers: " + metaJSON, ex);
            }
        }
    }

    public void processTowers() {
        if (latitude == 0 && longitude == 0) {
            return;
        }
        if (waitingForGPS != null) {
            waitingForGPS.dismiss();
            waitingForGPS = null;
        }
        visibleTowers.clear();
        for (Tower tower : allTowers) {
            tower.distance = Geo.distance(latitude, longitude, tower.latitude, tower.longitude);
            tower.azimuth = Geo.azimuth(latitude, longitude, tower.latitude, tower.longitude);
            if (tower.distance < maxDistance / currentScale) {
                tower.x = tower.distance * Math.sin(tower.azimuth * Math.PI / 180.);
                tower.y = tower.distance * Math.cos(tower.azimuth * Math.PI / 180.);
                visibleTowers.add(tower);
            }
        }
        for (Tower tower : visibleTowers) {
            if (tower.disabled) {
                continue;
            }
        }
    }

    public void refresh() {
        towersJSON = sharedPref.getString(SeetowersClient.STATE_DATA(state), "[]");
        metaJSON = sharedPref.getString(SeetowersClient.STATE_META(state), "{}");
        mTowersView.units = Units.safeValueOf(sharedPref.getString("units", Units.mi.name()));
        parseTowers();
        processTowers();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorAccelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mSensorMagneticField, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mSensorOrientation, SensorManager.SENSOR_DELAY_UI);
        refresh();
    }
    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this, mSensorAccelerometer);
        mSensorManager.unregisterListener(this, mSensorMagneticField);
        mSensorManager.unregisterListener(this, mSensorOrientation);
        /*
        try {
            mLocationManager.removeUpdates(this);
        } catch (SecurityException ex) {
            Log.e("error", "Location services are not enabled", ex);
        }
        */
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    private void toggle() {
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            mCompassView.mAccuracy = accuracy;
            mCompassView.invalidate();
        }
    }

    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                System.arraycopy(event.values, 0, mValuesAccelerometer, 0, 3);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                System.arraycopy(event.values, 0, mValuesMagneticField, 0, 3);
                break;
            case Sensor.TYPE_ORIENTATION:
                System.arraycopy(event.values, 0, mValuesOrientation, 0, 3);
                break;
        }

        boolean success = SensorManager.getRotationMatrix(mMatrixR, mMatrixI,
                mValuesAccelerometer,
                mValuesMagneticField);

        // calculate a new smoothed azimuth value and store to mAzimuth
        if (success) {
            SensorManager.getOrientation(mMatrixR, mMatrixValues);
            mAzimuthRadians.putValue(mMatrixValues[0]);
            mAzimuth = Math.toDegrees(mAzimuthRadians.getAverage());
        }
        updateBearing();
    }

    @Override
    public void onLocationChanged(Location location) {
        // set the new location
        this.mLocation = location;
        this.longitude = mLocation.getLongitude();
        this.latitude = mLocation.getLatitude();

        if (System.currentTimeMillis() - mLastLocationUpdatedAt > mLocationUpdateThrottle) {
            mLastLocationUpdatedAt = System.currentTimeMillis();

            processTowers();
            mTowersView.visibleTowers = visibleTowers;
            mTowersView.invalidate();
        }
    }

    private void updateBearing()
    {
        if (!Double.isNaN(this.mAzimuth)) {
            if(this.mLocation == null) {
                mBearing = mAzimuth;
            } else {
                mBearing = getBearingForLocation(this.mLocation);
            }
            if( System.currentTimeMillis() - mLastChangeDispatchedAt > mThrottleTime &&
                    (Double.isNaN(mLastBearing) || Math.abs(mLastBearing - mBearing) >= mMinDiffForEvent)) {
                mLastBearing = mBearing;
                mLastChangeDispatchedAt = System.currentTimeMillis();


                if (mTowersView.target != null) {
                    Log.d("Bearing", "target=" + String.valueOf(mTowersView.target.azimuth) + " phone=" + mBearing);
                    if (Math.abs(mBearing - mTowersView.target.azimuth) <= 5) {
                        mTowersView.setBackgroundColor(0xFF05202c);
                    } else {
                        mTowersView.setBackgroundColor(Color.BLACK);
                    }
                }

                mCompassView.mBearing = mLastBearing;
                mCompassView.invalidate();
                mTowersView.invalidate();
            }
        }
    }

    private double getBearingForLocation(Location location) {
        return mAzimuth + getGeomagneticField(location).getDeclination();
    }

    private GeomagneticField getGeomagneticField(Location location) {
        GeomagneticField geomagneticField = new GeomagneticField(
                (float)location.getLatitude(),
                (float)location.getLongitude(),
                (float)location.getAltitude(),
                System.currentTimeMillis());
        return geomagneticField;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
