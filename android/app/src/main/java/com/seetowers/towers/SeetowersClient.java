package com.seetowers.towers;

import android.app.Activity;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ppeshin on 3/3/16.
 */
public class SeetowersClient {

    private String endpoint = "https://seetowers.com/";

    private static final String META = "var Meta = ";
    private static final String DATA = "var TowersData = ";
    private static final String STATES = "var States = ";
    private static final String META_END = "};";
    private static final String DATA_END = "]];";
    private static final String STATES_END = "];";
    private static final String MANIFEST = "/manifest.appcache";

    public static final String META_KEY = "meta";
    public static final String DATA_KEY = "data";
    public static final String STATES_KEY = "states";
    public static final String HASH_KEY = "hash";
    public static String STATE_HASH(String state) { return HASH_KEY + "_" + state; }
    public static String STATE_DATA(String state) { return DATA_KEY + "_" + state; }
    public static String STATE_META(String state) { return META_KEY + "_" + state; }


    public void getTowers(String state, String hash, Activity activity, SuccessListener onSuccess, ErrorListener onError, Runnable onExit) {
        HttpRequestTask task = new HttpRequestTask();
        task.activity = activity;
        task.onSuccess = onSuccess;
        task.onError = onError;
        task.onExit = onExit;
        task.hash = hash;
        task.responseParser = new ResponseParser() {
            @Override
            public void parse(StringBuffer buf, Map<String, String> data) {
                int metaIndex = buf.indexOf(META);
                int metaEnd = buf.indexOf(META_END, metaIndex) + META_END.length() - 1;
                int dataIndex = buf.indexOf(DATA);
                int dataEnd = buf.indexOf(DATA_END, dataIndex) + DATA_END.length() - 1;

                data.put(META_KEY, buf.substring(metaIndex + META.length(), metaEnd));
                data.put(DATA_KEY, buf.substring(dataIndex + DATA.length(), dataEnd));
            }
        };
        task.execute(state + MANIFEST, state + "/towers-data.js");
    }

    public void getStates(String hash, Activity activity, SuccessListener onSuccess, ErrorListener onError, Runnable onExit) {
        HttpRequestTask task = new HttpRequestTask();
        task.activity = activity;
        task.onSuccess = onSuccess;
        task.onError = onError;
        task.onExit = onExit;
        task.hash = hash;
        task.responseParser = new ResponseParser() {
            @Override
            public void parse(StringBuffer buf, Map<String, String> data) {
                int statesIndex = buf.indexOf(STATES);
                int statesEnd = buf.indexOf(STATES_END, statesIndex) + STATES_END.length() - 1;
                data.put(STATES_KEY, buf.substring(statesIndex + STATES.length(), statesEnd));
            }
        };
        task.execute(MANIFEST, "/index.js");
    }

    public interface ErrorListener {
        void run(String result);
    }

    public interface SuccessListener {
        void run(Map<String, String> result);
    }

    public interface ResponseParser {
        void parse(StringBuffer buf, Map<String, String> data);
    }

    class HttpRequestTask extends AsyncTask<String, Integer, Boolean> {

        private Activity activity;

        private SuccessListener onSuccess;

        private ErrorListener onError;

        private Runnable onExit;

        private String hash;

        private ResponseParser responseParser;

        protected Boolean doInBackground(String... objs) {
            HttpURLConnection connection = null;
            Boolean result;
            try {
                final Map<String, String> data = new HashMap<>();
                for (String obj : objs) {
                    URL url = new URL(endpoint + obj);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setUseCaches(false);
                    BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    final StringBuffer buf = new StringBuffer();
                    String line;
                    while (null != (line = rd.readLine())) {
                        buf.append(line);
                    }
                    rd.close();
                    if (obj.endsWith(MANIFEST)) {
                        String newHash = String.valueOf(buf.toString().hashCode());
                        if (newHash.equals(hash)) {
                            break;
                        } else {
                            data.put(HASH_KEY, newHash);
                        }
                    } else {
                        responseParser.parse(buf, data);
                    }
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        onSuccess.run(data);
                    }
                });
                result = Boolean.TRUE;
            } catch (final Exception e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        onError.run(e.getMessage());
                    }
                });
                result = Boolean.FALSE;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        onExit.run();
                    }
                });
            }
            return result;
        }
    }
}
