Number.prototype.round = function(places) {
    return + (Math.round(this + "e+" + places)  + "e-" + places);
}

Number.prototype.toRad = function() {
    return this * Math.PI / 180;
}

var screenWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
var canvasScale = 2;

function refreshLocation() {
    if (navigator.geolocation) {
        $('#wait').show();
        $('#data').hide();
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.")
    }
}

function showPosition(position) {
    var mylat = position.coords.latitude;
    var mylon = position.coords.longitude;
    $('#location').val(mylat.round(4) + ',' + mylon.round(4));
    render(mylat, mylon);
}

function getParameterByName(url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function parseLocation() {
    var portalLocation = getParameterByName($('#location').val(), "pll");
    var result;
    if (portalLocation) {
        result = portalLocation;
    } else {
        result = $('#location').val();
    }    
    return result.split(/[ ,]+/);
}

function refreshTable() {
    $('#wait').show();
    $('#data').hide();
    var location = parseLocation();
    render(parseFloat(location[0]), parseFloat(location[1]));
}

function view_more() {
    var max_distance = parseFloat($('#max_distance').val());
    max_distance = max_distance + 1;
    $('#max_distance').val(max_distance);
    refreshTable();
}

function view_less() {
    var max_distance = parseFloat($('#max_distance').val());
    if (max_distance > 5) {
        max_distance = max_distance - 1;
        $('#max_distance').val(max_distance);
        refreshTable();
    }
}

var rendered_towers = [];

function markTower(towerId) {
    $('#tower_' + towerId + ' td').css('background-color', '#EEE');
    for (var i in rendered_towers) {
        var t = rendered_towers[i];
        if (t.id == towerId) {
            var canvas = $("#canvas");
            var ctx = canvas[0].getContext("2d");
            ctx.beginPath();
            ctx.arc(t.x, t.y, 21, 0, 2*Math.PI);
            ctx.strokeStyle = "red";
            ctx.lineWidth = 4;
            ctx.stroke();
            break;
        }
    }
}

function render(mylat, mylon) {
    $('#wait').hide();
    $('#data').show();
    $('#meta').text(Meta.stateName);
    $('#data tbody').html('');
    var max_distance = parseFloat($('#max_distance').val());
    var towers = Towers.proximity(mylat, mylon);
    
    var size = $('#header').width();
    var canvas = $("#canvas");
    canvas.width(size);
    canvas.height(size);
    canvas.attr("width", size * canvasScale);
    canvas.attr("height", size * canvasScale);
    
    var width = canvas.width() * canvasScale;
    var height = canvas.height() * canvasScale;
    var ctx = canvas[0].getContext("2d");
    var radius = Math.min(width/2, height/2);
    var x0 = width/2;
    var y0 = height/2;
    ctx.textAlign = "center";
    ctx.font = "normal 100 20px sans-serif";
    ctx.clearRect(0, 0, width, height);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.setLineDash([3, 3]);
    // ctx.arc(x0, y0, radius, 0, 2*Math.PI);
    
    var radiusStep = max_distance < 16 ? 2 : (max_distance < 36 ? 5 : 10);
    for (var r = radiusStep; r < max_distance; r += radiusStep) {
        var rr = r * radius/max_distance;
        ctx.arc(x0, y0, rr, 0, 2*Math.PI);
    }
    
    for (var a = 0; a < 360; a += 30) {
        var rr = radiusStep * radius/max_distance;
        var xx1 = x0 + (radius - 25) * Math.sin(a.toRad());
        var yy1 = y0 + (radius - 25) * Math.cos(a.toRad());
        var xx2 = x0 + rr * Math.sin(a.toRad());
        var yy2 = y0 + rr * Math.cos(a.toRad());
        ctx.moveTo(xx1, yy1);
        ctx.lineTo(xx2, yy2);
    }
    
    ctx.moveTo(x0 - radius + 40, y0);
    ctx.lineTo(x0 + radius - 40, y0);
    ctx.moveTo(x0, y0 - radius + 40);
    ctx.lineTo(x0, y0 + radius - 40);

    ctx.stroke();
    
    ctx.setLineDash([]);
    for (var r = radiusStep; r < max_distance; r += radiusStep) {
        var rr = r * radius/max_distance;
        ctx.strokeText(r, x0 + rr + 12, y0 + 18);
    }

    for (var r = radiusStep; r < max_distance; r += radiusStep) {
        var rr = r * radius/max_distance;
        ctx.strokeText(r, x0 - rr - 12, y0 - 8);
    }

    for (var a = 0; a < 360; a += 30) {
        var rr = 2 * radius/max_distance;
        var xx1 = x0 + (radius - 8) * Math.sin(a.toRad());
        var yy1 = y0 - (radius - 8) * Math.cos(a.toRad());
        ctx.strokeText(a, xx1, yy1 + 8);
    }

    
    
    rendered_towers = [];
    var nearest_towers = [];
    for(var i in towers) {
        var t = towers[i];
        if (t.distance > max_distance) {
            break;
        }
        nearest_towers.push(t);
    }
    
    nearest_towers.sort(function(a, b) {
        return a.azimuth - b.azimuth;
    });

    for(var i in nearest_towers) {
        var t = nearest_towers[i];
        if (t.distance > max_distance) {
            break;
        }
        var x = t.distance * Math.sin(t.azimuth.toRad());
        var y = t.distance * Math.cos(t.azimuth.toRad());
        
        var xx = x0 + x * radius/max_distance;
        var yy = y0 - y * radius/max_distance;
        
        var towerId = parseInt(i) + 1;
        ctx.beginPath();
        ctx.arc(xx, yy, 20, 0, 2*Math.PI);
        ctx.fillStyle = "#6495ED";
        ctx.fill();
        rendered_towers.push({id: towerId, x: xx, y: yy});
        ctx.strokeText(towerId.toString(), xx, yy + 6);
        $('#data tbody').append('<tr class="tower" id="tower_' + towerId + '">' 
            + '<td class="tower_id">' + towerId + '</td>'
            + '<td class="azimuth">' + t.azimuth.round(1) + '</td>'
            + '<td class="distance">' + t.distance.round(3) + '</td>'
            + '<td>' 
            + '<div class="name">' + TowersData[t.index][2] + '</div>'
            + '<div class="location">' + TowersData[t.index][3] + ', ' + TowersData[t.index][4] + '</div>'
            + '</td>'
            + '</tr>');
    }      
    $('tr.tower td').click(function(e) {
        var towerId = $(this).parent().attr('id').substring(6);
        markTower(towerId);
    });
}

function canvasDistance(x1, y1, x2, y2) {
    return Math.sqrt( (x2-=x1)*x2 + (y2-=y1)*y2 );
}

$(document).ready(function() {
    $('#canvas').width(screenWidth);
    $('#data').width(screenWidth);
    $('#header').width(screenWidth);
    
    refreshLocation();
    //$('#location').val("37.3187,-122.0028");
    //refreshTable();
    
    $("#canvas").click(function(e) {
        var mouseX = e.pageX - this.offsetLeft;
        var mouseY = e.pageY - this.offsetTop;
        for (var i in rendered_towers) {
            var t = rendered_towers[i];
            if (canvasDistance(mouseX * canvasScale, mouseY * canvasScale, t.x, t.y) < 30) {
                markTower(t.id);
            }
        }
    });
});

