/*
 * Credits
 * http://www.codeguru.com/cpp/cpp/algorithms/article.php/c5115/Geographic-Distance-and-Azimuth-Calculations.htm
 * Geographic Distance and Azimuth Calculations
 * Posted by Andy McGovern on March 26th, 2003
 * 
 * Translated to Javascript by Philip Peshin 
 * August 15, 2015
 */

if (typeof Geo === "undefined") {
    var Geo = {};
}

Geo.PI                = 3.14159265359;
Geo.TWOPI             = 6.28318530718;
Geo.DE2RA             = 0.01745329252;
Geo.RA2DE             = 57.2957795129;
Geo.ERAD              = 6378.135;
Geo.ERADM             = 6378135.0;
Geo.AVG_ERAD          = 6371.0;
Geo.FLATTENING        = 1.0/298.257223563;
Geo.EPS               = 0.000000000005;
Geo.KM2MI             = 0.621371;
Geo.GEOSTATIONARY_ALT = 35786.0;

Geo.distance = function(lat1, lon1, lat2, lon2) {
    lat1 *= Geo.DE2RA;
    lon1 *= Geo.DE2RA;
    lat2 *= Geo.DE2RA;
    lon2 *= Geo.DE2RA;
    var d = Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2);
    return (Geo.AVG_ERAD * Math.acos(d));   
}

Geo.azimuth = function(lat1, lon1, lat2, lon2) {
    var result = 0.0;

    var ilat1 = Math.floor(0.50 + lat1 * 360000.0);
    var ilat2 = Math.floor(0.50 + lat2 * 360000.0);
    var ilon1 = Math.floor(0.50 + lon1 * 360000.0);
    var ilon2 = Math.floor(0.50 + lon2 * 360000.0);

    lat1 *= Geo.DE2RA;
    lon1 *= Geo.DE2RA;
    lat2 *= Geo.DE2RA;
    lon2 *= Geo.DE2RA;

    if ((ilat1 == ilat2) && (ilon1 == ilon2)) {
        return result;
    } else if (ilon1 == ilon2) {
        if (ilat1 > ilat2)
            result = 180.0;
    } else {
        var c = Math.acos(Math.sin(lat2) * Math.sin(lat1) + Math.cos(lat2) * Math.cos(lat1)
                * Math.cos((lon2 - lon1)));
        var A = Math.asin(Math.cos(lat2) * Math.sin((lon2 - lon1)) / Math.sin(c));
        result = (A * Geo.RA2DE);

        if ((ilat2 > ilat1) && (ilon2 > ilon1)) {
        } else if ((ilat2 < ilat1) && (ilon2 < ilon1)) {
            result = 180.0 - result;
        } else if ((ilat2 < ilat1) && (ilon2 > ilon1)) {
            result = 180.0 - result;
        } else if ((ilat2 > ilat1) && (ilon2 < ilon1)) {
            result += 360.0;
        }
    }
    return result;
}