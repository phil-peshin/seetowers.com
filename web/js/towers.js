if (typeof Towers === "undefined") {
    var Towers = {};
}

Towers.proximity = function (mylat, mylon) {
    var result = [];
    for (var index in TowersData) {
        var t = TowersData[index];
        result.push({
            distance: Geo.distance(mylat, mylon, t[0], t[1]), 
            azimuth: Geo.azimuth(mylat, mylon, t[0], t[1]),
            index: index 
        })
    }
    
    result.sort(function(a, b) {
        return a.distance - b.distance;
    });
    return result;
}

