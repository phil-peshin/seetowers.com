var States = [
    ["/us/ak/", "Alaska"],
    ["/us/az/", "Arizona"],
    ["/ca/bc/", "British Columbia"],
    ["/us/ca/", "California"],
    ["/us/co/", "Colorado"],
    ["/us/hi/", "Hawaii"],
    ["/us/id/", "Idaho"],
    ["/us/il/", "Illinois"],
    ["/us/id/", "Indiana"],
    ["/us/mi/", "Michigan"],
    ["/us/mt/", "Montana"],
    ["/us/nv/", "Nevada"],
    ["/us/oh/", "Ohio"],
    ["/us/or/", "Oregon"],
    ["/us/pa/", "Pennsylvania"],
    ["/us/ut/", "Utah"],
    ["/us/wa/", "Washington"]
];
