package com.seetowers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Canada {

    static double PI = 3.14159265359;
    static double TWOPI = 6.28318530718;
    static double DE2RA = 0.01745329252;
    static double RA2DE = 57.2957795129;
    static double ERAD = 6378.135;
    static double ERADM = 6378135.0;
    static double AVG_ERAD = 6371.0;
    static double FLATTENING = 1.0 / 298.257223563;
    // Earth flattening (WGS '84)
    static double EPS = 0.000000000005;
    static double KM2MI = 0.621371;
    static double GEOSTATIONARY_ALT = 35786.0; // km

    public static class Tower {
        double lat;
        double lon;
        String name;
        String city;

        double distance;
        double direction;

        public Tower(double lat, double lon, String name, String city) {
            super();
            this.lat = lat;
            this.lon = lon;
            this.name = name;
            this.city = city;
        }

        @Override
        public String toString() {
            return "Tower [lat=" + lat + ", lon=" + lon + ", name=" + name + ", city=" + city
                    + ", distance=" + distance + ", direction=" + direction + "]";
        }

        public String jsSafe(String s) {
            return s != null ? s.replace('\'', '`') : "";
        }

        public String toJSON() {
            return "[" + lat + "," + lon + ",'" + jsSafe(name) + "','" + jsSafe(city) + "','']";
        }
    }



    public static void main(String[] args) throws Exception {
        File data = new File("data/bc.txt");
        BufferedReader r = new BufferedReader(new FileReader(data));

        String[] state = {"bc", "", "", "", "British Columbia"};
        PrintWriter kml = new PrintWriter(new FileWriter("output/towers-data-" + state[0] + ".kml"));
        kml.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n");
        kml.println("<Document><name>FCC US cell towers</name><open>1</open>");
        kml.println("<Folder><name>" + state[4] + "</name>");
        kml.println("<Style id='icon'><IconStyle><Icon><href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png</href></Icon></IconStyle></Style>");

        List<Tower> towers = new ArrayList<Tower>();
        PrintWriter output = new PrintWriter(new FileWriter("output/towers-data-" + state[0] + ".js"));
        output.println("var Meta = { "
                + "stateCode: '" + ((String)state[0]).toUpperCase() + "', "
                + "stateName: '" + state[4] + "', "
                + "latitude: " + state[1] + ", "
                + "longitude: " + state[2] + ", "
                + "radius: " + state[3] + " };");
        output.println("var TowersData = [");
        int count = 0;

        while (true) {
            String line = r.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#") || line.trim().length() == 0) {
                continue;
            }
            String s_lat = line.substring(32, 38);
            String s_lon = line.substring(39, 46);
            double lat = Double.parseDouble(s_lat.substring(0, 2)) + Double.parseDouble(s_lat.substring(2, 4))/60 + Double.parseDouble(s_lat.substring(4))/3600;
            double lon = -1 * (Double.parseDouble(s_lon.substring(0, 3)) + Double.parseDouble(s_lon.substring(3, 5))/60 + Double.parseDouble(s_lon.substring(5))/3600);
            String city = line.substring(47, 82);
            String entity = line.substring(128, 163);

            Tower t = new Tower(lat, lon, entity, city);
            towers.add(t);
            if (count != 0) {
                output.println(",");
            }
            output.print(t.toJSON());

            count++;
            kml.println(
                    "<Placemark>"
                  + "<styleUrl>#icon</styleUrl>"
                  + "<name>" + (t.name != null ? t.name.replace('&', ' ') : "") + "</name>"
                  + "<Point>" + "<coordinates>" + t.lon + "," + t.lat + ",0</coordinates>" + "</Point>"
                  + "</Placemark>");
        }
        output.println("];");
        output.close();

        kml.println("</Folder>");
        kml.println("</Document>");
        kml.println("</kml>");
        kml.close();
    }

    static double d(Object o) {
        return ((Double) o).doubleValue();
    }

    static double GCDistance(double lat1, double lon1, double lat2, double lon2) {
        lat1 *= DE2RA;
        lon1 *= DE2RA;
        lat2 *= DE2RA;
        lon2 *= DE2RA;
        double d = Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2);
        return (AVG_ERAD * Math.acos(d));
    }

    static double GCAzimuth(double lat1, double lon1, double lat2, double lon2) {
        double result = 0.0;

        int ilat1 = (int) (0.50 + lat1 * 360000.0);
        int ilat2 = (int) (0.50 + lat2 * 360000.0);
        int ilon1 = (int) (0.50 + lon1 * 360000.0);
        int ilon2 = (int) (0.50 + lon2 * 360000.0);

        lat1 *= DE2RA;
        lon1 *= DE2RA;
        lat2 *= DE2RA;
        lon2 *= DE2RA;

        if ((ilat1 == ilat2) && (ilon1 == ilon2)) {
            return result;
        } else if (ilon1 == ilon2) {
            if (ilat1 > ilat2)
                result = 180.0;
        } else {
            double c = Math.acos(Math.sin(lat2) * Math.sin(lat1) + Math.cos(lat2) * Math.cos(lat1)
                    * Math.cos((lon2 - lon1)));
            double A = Math.asin(Math.cos(lat2) * Math.sin((lon2 - lon1)) / Math.sin(c));
            result = (A * RA2DE);

            if ((ilat2 > ilat1) && (ilon2 > ilon1)) {
            } else if ((ilat2 < ilat1) && (ilon2 < ilon1)) {
                result = 180.0 - result;
            } else if ((ilat2 < ilat1) && (ilon2 > ilon1)) {
                result = 180.0 - result;
            } else if ((ilat2 > ilat1) && (ilon2 < ilon1)) {
                result += 360.0;
            }
        }

        return result;
    }

}
