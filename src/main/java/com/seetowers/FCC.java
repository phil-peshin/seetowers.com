package com.seetowers;

import org.apache.commons.cli.*;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FCC {
    public static int MAX_BATCH = 256;

    static Object[][] states = {
        // state, lat, long, radius
        { "ca", 37.1992, -119.4505, 700., "California" },
        { "or", 43.9336, -120.5617, 500., "Oregon" },
        { "wa", 47.3822, -120.4505, 500., "Washington" },
        { "id", 44.3538, -114.6086, 700., "Idaho" },
        { "ut", 39.3057, -111.6685, 500., "Utah" },
        { "nv", 39.3306, -116.6268, 600., "Nevada" },
        { "mt", 47.0527, -109.6450, 700., "Montana" },
        { "az", 34.2740, -111.6582, 500., "Arizona" },
        { "ak", 64.731667, -152.47, 1000., "Alaska" },
        { "co", 39.0000, -105.547222, 500., "Colorado" },
        { "hi", 21.133333, -157.033333, 400., "Hawaii" },
        { "mi", 44.262311, -85.392573, 700., "Michigan" },
        { "oh", 40.098266, -82.912593, 400., "Ohio" },
        { "in", 39.781725, -86.393029, 400., "Indiana" },
        { "il", 39.749003, -89.545874, 500., "Illinois" },
        { "pa", 40.902980, -77.789311, 400., "Pennsylvania"}
    };

    static double PI = 3.14159265359;
    static double TWOPI = 6.28318530718;
    static double DE2RA = 0.01745329252;
    static double RA2DE = 57.2957795129;
    static double ERAD = 6378.135;
    static double ERADM = 6378135.0;
    static double AVG_ERAD = 6371.0;
    static double FLATTENING = 1.0 / 298.257223563;
    // Earth flattening (WGS '84)
    static double EPS = 0.000000000005;
    static double KM2MI = 0.621371;
    static double GEOSTATIONARY_ALT = 35786.0; // km

    public static class Tower {
        double lat;
        double lon;
        String name;
        String city;
        String state;

        double distance;
        double direction;

        public Tower(double lat, double lon, String name, String city, String state) {
            super();
            this.lat = lat;
            this.lon = lon;
            this.name = name;
            this.city = city;
            this.state = state;
        }

        @Override
        public String toString() {
            return "Tower [lat=" + lat + ", lon=" + lon + ", name=" + name + ", city=" + city + ", state=" + state
                    + ", distance=" + distance + ", direction=" + direction + "]";
        }

        public String jsSafe(String s) {
            return s != null ? s.replace('\'', '`') : "";
        }

        public String toJSON() {
            return "[" + lat + "," + lon + ",'" + jsSafe(name) + "','" + jsSafe(city) + "','" + jsSafe(state) + "']";
        }
    }

    public static String getResourceFileAsString(String resourceFileName) {
        InputStream is = FCC.class.getClassLoader().getResourceAsStream(resourceFileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        return reader.lines().collect(Collectors.joining("\n"));
    }

    public static String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    }

    public static void downloadFromUrl(URL url, String localFilename) throws IOException {
        InputStream is = null;
        FileOutputStream fos = null;

        try {
            URLConnection urlConn = url.openConnection();//connect

            is = urlConn.getInputStream();               //get connection inputstream
            fos = new FileOutputStream(localFilename);   //open outputstream to local file

            byte[] buffer = new byte[4096];              //declare 4KB buffer
            int len;

            //while we have availble data, continue downloading and storing to local file
            while ((len = is.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } finally {
                if (fos != null) {
                    fos.close();
                }
            }
        }
    }

    public static final String QUERY =
            "select distinct "
            + "RA.STRUCTURE_CITY, "
            + "RA.STRUCTURE_STATE_CODE, "
            + "(SELECT GROUP_CONCAT(DISTINCT EN.entity_name SEPARATOR ', ') FROM TOWER_PUBACC_EN EN WHERE EN.unique_system_identifier = CO.unique_system_identifier), "
            + "CO.latitude_total_seconds/3600 * (case when latitude_direction = 'N' then 1 else -1 end), "
            + "CO.longitude_total_seconds/3600 * (case when longitude_direction = 'E' then 1 else -1 end) "
            + "FROM TOWER_PUBACC_CO CO "
            + "JOIN TOWER_PUBACC_RA RA ON CO.unique_system_identifier = RA.UNIQUE_SYSTEM_IDENTIFIER "
            + "WHERE RA.status_code = 'C'";

    public static void main(String[] args) throws Exception {
        Options options = new Options();

        Option usernameOption = new Option("u", "username", true, "database user");
        usernameOption.setRequired(false);
        options.addOption(usernameOption);

        Option passwordOption = new Option("p", "password", true, "database password");
        passwordOption.setRequired(false);
        options.addOption(passwordOption);

        Option hostOption = new Option("h", "host", true, "database hostname");
        hostOption.setRequired(false);
        options.addOption(hostOption);

        Option inputOption = new Option("i", "input", true, "input");
        inputOption.setRequired(false);
        options.addOption(inputOption);

        Option outputOption = new Option("o", "output", true, "output folder");
        outputOption.setRequired(false);
        options.addOption(outputOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("fcc", options);

            System.exit(1);
            return;
        }

        Class.forName("com.mysql.jdbc.Driver").newInstance();

        String inputURL = cmd.getOptionValue("input", "http://wireless.fcc.gov/uls/data/complete/r_tower.zip");
        System.out.println("DOWNLOADING: " + inputURL);

        File tempFile = File.createTempFile("r_tower", ".zip");
        tempFile.deleteOnExit();
        downloadFromUrl(new URL(inputURL), tempFile.getAbsolutePath());

        String db = "r_tower";
        try (ZipFile zip = new ZipFile(tempFile.getAbsolutePath());
                Connection conn = DriverManager.getConnection("jdbc:mysql://" + cmd.getOptionValue("host", "localhost") + "/" + db + "?user=" + cmd.getOptionValue("username", "root") + "&password=" + cmd.getOptionValue("password", ""))) {
            List<ZipEntry> files = zip.stream().filter(e -> e.getName().endsWith(".dat")).collect(Collectors.toList());
            conn.setAutoCommit(false);
            for (ZipEntry f : files) {
                String tableName = "TOWER_PUBACC_" + f.getName().substring(0, f.getName().indexOf('.'));
                try (Statement st = conn.createStatement()) {
                    st.execute("TRUNCATE " + tableName);
                    conn.commit();
                }
                StringBuffer insertStatement = new StringBuffer();
                List<Integer> columnTypes = new ArrayList<Integer>();
                List<String> columnTypeNames = new ArrayList<String>();
                try (PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + tableName);
                        ResultSet rs = ps.executeQuery()) {
                    ResultSetMetaData md = rs.getMetaData();
                    int columnCount = md.getColumnCount();
                    insertStatement.append("INSERT INTO ").append(tableName).append("(");
                    for (int c = 1; c <= columnCount; c++) {
                        columnTypes.add(md.getColumnType(c));
                        columnTypeNames.add(md.getColumnTypeName(c));
                        insertStatement.append(md.getColumnName(c));
                        if (c < columnCount) {
                            insertStatement.append(",");
                        }
                    }
                    insertStatement.append(") VALUES (");
                    columnTypes.forEach(s -> insertStatement.append("?,"));
                    insertStatement.deleteCharAt(insertStatement.length() - 1);
                    insertStatement.append(")");
                }
                System.out.println("WRITING TABLE: " + tableName);
                try (BufferedReader bin = new BufferedReader(new InputStreamReader(zip.getInputStream(f)));
                        PreparedStatement ps = conn.prepareStatement(insertStatement.toString())) {
                    String line;
                    int totalCounter = 0;
                    int batchCounter = 0;
                    while ((line = bin.readLine()) != null) {
                        String[] columns = line.split("\\|", -1);
                        if (columns.length < columnTypes.size()) {
                            System.out.println("Try reading next for: " + line);
                            String line2 = "";
                            while (true) {
                                line2 = bin.readLine();
                                System.out.println("Segment: " + line2);
                                if (line2 != null && line2.trim().length() == 0) {
                                    continue;
                                }
                                break;
                            }
                            line += "; " + line2;
                            System.out.println("Read: " + line);
                            columns = line.split("\\|", -1);
                        }
                        if (columns.length != columnTypes.size()){
                            System.out.println("SKIP BAD LINE: " + line);
                            continue;
                        }
                        int index = 0;
                        for (int type : columnTypes) {
                            int param = index + 1;
                            if (columns[index].length() == 0) {
                                ps.setNull(param, type);
                            } else {
                                switch (type) {
                                case Types.VARCHAR:
                                case Types.CHAR:
                                    ps.setString(param, columns[index]);
                                    break;
                                case Types.DECIMAL:
                                case Types.NUMERIC:
                                    ps.setDouble(param, Double.parseDouble(columns[index]));
                                    break;
                                case Types.INTEGER:
                                    ps.setInt(param, Integer.parseInt(columns[index]));
                                    break;
                                case Types.TIMESTAMP:
                                    ps.setDate(param, new Date(new SimpleDateFormat("dd/MM/yyyy").parse(columns[index]).getTime()));
                                    break;
                                default:
                                    System.out.println("Unknown type: " + columnTypeNames.get(index));
                                }
                            }
                            index++;
                        }
                        batchCounter++;
                        totalCounter++;
                        ps.addBatch();
                        if (batchCounter > MAX_BATCH) {
                            ps.executeBatch();
                            conn.commit();
                            batchCounter = 0;
                        }
                    }
                    if (batchCounter > 0) {
                        ps.executeBatch();
                        conn.commit();
                    }
                    System.out.println("Total: " + totalCounter);
                }
            }

            File kmlFolder = new File(cmd.getOptionValue("output", "output") + "/kml/");
            kmlFolder.mkdirs();

            for (Object[] state : states) {
                System.out.println("WRITING STATE: " + state[4]);
                PreparedStatement ps = conn.prepareStatement(QUERY);
                ResultSet rs = ps.executeQuery();

                PrintWriter kml = new PrintWriter(new FileWriter(kmlFolder.getAbsolutePath() + "/towers-data-" + state[0] + ".kml"));
                kml.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n");
                kml.println("<Document><name>FCC US cell towers</name><open>1</open>");
                kml.println("<Folder><name>" + state[4] + "</name>");
                kml.println("<Style id='icon'><IconStyle><Icon><href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png</href></Icon></IconStyle></Style>");

                List<Tower> towers = new ArrayList<Tower>();

                File jsFolder = new File(cmd.getOptionValue("output", "output") + "/us/" + state[0]);
                jsFolder.mkdirs();

                PrintWriter manifest = new PrintWriter(new FileWriter(jsFolder.getAbsolutePath() + "/manifest.appcache"));
                manifest.print(String.format(getResourceFileAsString("manifest.appcache"), getCurrentLocalDateTimeStamp()));

                PrintWriter output = new PrintWriter(new FileWriter(jsFolder.getAbsolutePath() + "/towers-data.js"));
                output.println("var Meta = { "
                        + "stateCode: '" + ((String)state[0]).toUpperCase() + "', "
                        + "stateName: '" + state[4] + "', "
                        + "latitude: " + state[1] + ", "
                        + "longitude: " + state[2] + ", "
                        + "radius: " + state[3] + " };");
                output.println("var TowersData = [");
                int count = 0;
                try {
                    while (rs.next()) {
                        String city = rs.getString(1);
                        String stateCode = rs.getString(2);
                        String entity = rs.getString(3);
                        double lat = rs.getDouble(4);
                        double lon = rs.getDouble(5);
                        Tower t = new Tower(lat, lon, entity, city, stateCode);
                        if (GCDistance(d(state[1]), d(state[2]), lat, lon) < d(state[3])
                                || ("ak".equals(state[0]) && "ak".equalsIgnoreCase(stateCode))
                                || ("hi".equals(state[0]) && "hi".equalsIgnoreCase(stateCode))
                                ) {
                            towers.add(t);
                            if (count != 0) {
                                output.println(",");
                            }
                            output.print(t.toJSON());

                            count++;
                        }
                        if ( String.valueOf(state[0]).equalsIgnoreCase(stateCode)) {
                            kml.println(
                                    "<Placemark>"
                                  + "<styleUrl>#icon</styleUrl>"
                                  + "<name>" + (t.name != null ? t.name.replace('&', ' ') : "") + "</name>"
                                  + "<Point>" + "<coordinates>" + t.lon + "," + t.lat + ",0</coordinates>" + "</Point>"
                                  + "</Placemark>");
                        }
                    }
                } finally {
                    rs.close();
                    ps.close();
                    output.println("];");
                    output.close();

                    kml.println("</Folder>");
                    kml.println("</Document>");
                    kml.println("</kml>");
                    kml.close();

                    manifest.close();
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    static double d(Object o) {
        return ((Double) o).doubleValue();
    }

    static double GCDistance(double lat1, double lon1, double lat2, double lon2) {
        lat1 *= DE2RA;
        lon1 *= DE2RA;
        lat2 *= DE2RA;
        lon2 *= DE2RA;
        double d = Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2);
        return (AVG_ERAD * Math.acos(d));
    }

    static double GCAzimuth(double lat1, double lon1, double lat2, double lon2) {
        double result = 0.0;

        int ilat1 = (int) (0.50 + lat1 * 360000.0);
        int ilat2 = (int) (0.50 + lat2 * 360000.0);
        int ilon1 = (int) (0.50 + lon1 * 360000.0);
        int ilon2 = (int) (0.50 + lon2 * 360000.0);

        lat1 *= DE2RA;
        lon1 *= DE2RA;
        lat2 *= DE2RA;
        lon2 *= DE2RA;

        if ((ilat1 == ilat2) && (ilon1 == ilon2)) {
            return result;
        } else if (ilon1 == ilon2) {
            if (ilat1 > ilat2)
                result = 180.0;
        } else {
            double c = Math.acos(Math.sin(lat2) * Math.sin(lat1) + Math.cos(lat2) * Math.cos(lat1)
                    * Math.cos((lon2 - lon1)));
            double A = Math.asin(Math.cos(lat2) * Math.sin((lon2 - lon1)) / Math.sin(c));
            result = (A * RA2DE);

            if ((ilat2 > ilat1) && (ilon2 > ilon1)) {
            } else if ((ilat2 < ilat1) && (ilon2 < ilon1)) {
                result = 180.0 - result;
            } else if ((ilat2 < ilat1) && (ilon2 > ilon1)) {
                result = 180.0 - result;
            } else if ((ilat2 > ilat1) && (ilon2 < ilon1)) {
                result += 360.0;
            }
        }

        return result;
    }

}
