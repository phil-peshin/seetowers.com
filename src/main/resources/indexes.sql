USE r_tower;

ALTER TABLE `TOWER_PUBACC_AT`
ADD INDEX `idx_TOWER_PUBACC_AT` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_CO`
ADD INDEX `idx_TOWER_PUBACC_CO` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_EN`
ADD INDEX `idx_TOWER_PUBACC_EN` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_HS`
ADD INDEX `idx_TOWER_PUBACC_HS` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_RA`
ADD INDEX `idx_TOWER_PUBACC_RA` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_RE`
ADD INDEX `idx_TOWER_PUBACC_RE` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_RS`
ADD INDEX `idx_TOWER_PUBACC_RS` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_SC`
ADD INDEX `idx_TOWER_PUBACC_SC` (`unique_system_identifier` ASC);

ALTER TABLE `TOWER_PUBACC_EC`
ADD INDEX `idx_TOWER_PUBACC_EC` (`unique_system_identifier` ASC);
